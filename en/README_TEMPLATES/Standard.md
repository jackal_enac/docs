<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=150px src="../../img/jackal.png" alt="Project logo"></a>
</p>

<h3 align="center">Jackal enac</h3>

<div align="center">



</div>

---

<p align="center"> Ce projet contient un ensemble de paquets ros qui permettent de controler le robot, construire une carte et y le localiser. 
    <br> 
</p>

## 📝 Table des matières
- [À propos](#about)
- [Pour commencer](#getting_started)
- [Paquets prérequis](#Prerequisites)
- [Utilisation](#usage)
- [Notes](#notes)
- [Développé en utilisant](#built_using)
- [Auteurs](#authors)
- [Contact](#contact)
- [Remerciement](#acknowledgement)

## 🧐 A propos <a name = "about"></a>
L'objectif de ce travail est de fournir à l'utilisateur des outils simples à integrer et à modifier au besoin, cette philosophie est renforcée par une documentation détaillée et un rapport qui explique le fonctionnement des algorithmes loam utilisés. En plus de la facilité de basculement entre un alogorithme loam à l'autre et la possibilté d'enregistrer les topics dans des fichiers .bag et les analyser en utilisant matlab, on propose aussi un fichier conf.h que l'utilisateur peut modifier sans aucune connaissances requises en programmation C/C++, en modifiant la valeurs des variables dans ce fichier, l'utilisateur modifie les contraintes prédéfinies pour classer un certain points comme un point surface ou un point de sol par exemple et donc modifie la performance de l'algorithme, cette performance peut ensuite etre calculer par la fonction matlab proposée. 


## 🏁 Pour Commener <a name = "getting_started"></a>
cloner le repertoire de git

``` 
cd ~/catkin_ws/src  #cd to your workspace directory
git clone ssh://git@git.recherche.enac.fr/signav/jackal_enac.git
catkin_make
```





## 🚀 Paquets prérequis <a name = "Prerequisites"></a>
Pour que l'ensemble de ces paquets fonctionnement correctement, il faut installer un ensemble de paquet dont ils dependent. 

L'algorithme aloam et lego_loam dépendent de : 
- [pcl](https://pointclouds.org/) - pointclouds 

```
sudo apt-get install libpcl-dev
```

- [pcl_ros](https://wiki.ros.org/pcl_ros) - pcl_ros 
sachant que ros noetic est la version que vous utilisez

``` 
sudo apt install ros-noetic-pcl-*
```


- [ceres](http://ceres-solver.org/) - ceres 

```
sudo apt-get install libceres-dev
```



- [Eigen3](https://eigen.tuxfamily.org/) - Eigen3 

```
sudo apt-get install libeigen3-dev
```


- [OpencV](https://opencv.org/) - Opencv

 ```
sudo apt-get install libopencv-dev
```



L'algorithme lego_loam dépend de :
- [gtsam](https://gtsam.org/) - gtsam 


```
git clone https://github.com/borglab/gtsam
cd gtsam
mkdir build
cd build
cmake ..
make install
```




## 🔧 Utilisation <a name = "Usage"></a>
 ```
roslaunch loam leg_loam.launch
```






## 🎈 Notes <a name="notes"></a>
L'algorithme lego_loam dépend d'une ancienne version d'eigen3, ainsi certaines fonctionnalités sont obslètes, si jamais vous rencontrez l'erreur suivant : 

error: ‘Index’ is not a member of ‘Eigen’ 340 | for (Eigen::Index ni = 0; ni < relative_coordinates.cols (); 


Juste modifiez la ligne ligne 340 et 669 du fichier /usr/include/pcl-1.10/pcl/filters/voxel_grid.h
Il faut juste utiliser int au lieu de Eigen::Index. 

avant:

for (Eigen::Index ni = 0; ni < relative_coordinates.cols (); ni++)

après:

for (int ni = 0; ni < relative_coordinates.cols (); ni++)


Pour effectuer ce changement il faut avoir l'accès root 







## ⛏️ Développé en utilisant <a name = "built_using"></a>
- [ROS](https://www.ros.org/) - ROS noetic
- [Jackal](https://www.clearpathrobotics.com/assets/guides/noetic/jackal/simulation.html) - Jackal simulator packages 
- [Gazebo](https://gazebosim.org/) - Gazebo
- [Gtsam](https://gtsam.org/) - gtsam
- [Lego_loam](https://github.com/RobustFieldAutonomyLab/LeGO-LOAM) - Lego_loam 
- [Aloam](https://github.com/HKUST-Aerial-Robotics/A-LOAM) - Aloam 

## ✍️ Auteurs <a name = "authors"></a>
- **Mouncef CHADILI** sous la supervision de **Jérémy VEZINET** 


## ✍️ Contact <a name = "contact"></a>
- mouncef.chadili@alumni.enac.fr  (ou mouncef@chadili.com)
- vezinet@recherche.enac.fr



## 🎉 Remerciement <a name = "acknowledgement"></a>
- Tixiao Shan developpeur de l'algorithme lego_loam 
- QIN Tong et CAO Shaozu développeurs de l'algorithme aloam  
